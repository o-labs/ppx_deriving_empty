# Empty values Ppx deriver

## Usage
This preprocessor derives default values for your types.
For example:
```ocaml
type r = {
  a: foo list;
  b: bar option;
  c: int;
  d: string;
} [@@deriving empty]
```
will produce a value:
```ocaml
let r_empty : r = { a = []; b = None; c = 0; d = "" }
```

## Options
For more accurate deriving, you can use options to customize your empty values or create functions.

### Global options
- `name`: string or identificator allowing to choose the name of the value derived.
- `debug`: flag that prints the value generated
- `labels`: flag that forces to generate a function with optional (or not) labels for all fields
- `cons`: flag or boolean to remember type to be derived in a later variant (if set to `true`, it will also produce the empty value)

```ocaml
type r = {
  a: foo list;
  b: bar option;
  c: int;
  d: string;
} [@@deriving empty {name=my_own_r; labels}]
```
will produce a value:
```ocaml
let my_own_r ?(a=[]) ?b ?(c=0) ?(d="") () = { a; b; c; d }
```

### Field options
- `arg`: flag that forces the field to be an anonymous argument
- `label`: flag, string or identificator that forces field to be a label (optional or not) with the given name (field name if not given)
- `empty` or `mt`: custom expression for the empty value

```ocaml
type r = {
  a: foo list; [@arg]
  b: bar option; [@label "my_b_label"]
  c: int; [@label my_c_label]
  d: string;
} [@@deriving empty]
```
will produce a value:
```ocaml
let r_empty ?my_by_label:b ?(d="") ~my_c_label:c  a = { a; b; c; d }
```

### Constructor options
- `cons`: flag or string or identifier that forces to produce the value for this constructor (the string or identifier will be the name for the value if set)

```ocaml
type r = {
  a: foo list;
  b: bar option;
  c: int;
  d: string;
} [@@deriving empty {cons}]

type v =
  | A of r [@cons my_v_a]
  | B of string
[@@deriving empty]
```
will produce a value:
```ocaml
let my_v_a = A { a = []; b = None; c = 0; d = "" }
```
