open Ppxlib
open Ast_builder.Default
open Utils

type inherit_fields =
  class_field list * (arg_label * pattern * core_type * expression option) list

let inherit_fields : (string, inherit_fields) Hashtbl.t = Hashtbl.create 512

let rec core ?empty ?arg ?name ?labels c =
  let loc = c.ptyp_loc in
  let aux (e, vars) = match arg with
    | Some n -> evar ~loc n,  vars @ [Optional n, pvar ~loc n, c, Some e]
    | None -> e, vars in
  let {empty; _} = get_empty_attrs ?empty c.ptyp_attributes in
  match empty with
  | Some e -> aux (e, [])
  | _ ->
    match c.ptyp_desc, name with
    | Ptyp_any, _ -> raise_error ~loc "any type not handled"
    | Ptyp_var v, _ -> aux @@
      (evar ~loc ("_" ^ em_name v),
       [ Nolabel, pvar ~loc ("_" ^ em_name v), ptyp_var ~loc v, None ])
    | Ptyp_constr ({txt; loc}, [ c ]), Some n
      when (Longident.name txt = "option" || Longident.name txt = "Option.t") && Option.is_some arg ->
      evar ~loc n, [Optional n, pvar ~loc n, c, None]
    | Ptyp_constr ({txt; loc}, l), _ ->
      aux (constr ~loc (Longident.name txt) l)
    | Ptyp_tuple l, _ ->
      let l, vars = acc_map core l in
      aux (pexp_tuple ~loc l, vars)
    | Ptyp_variant (l, _, _), _ -> aux (variant l)
    | Ptyp_arrow (al, _, c), _ ->
      let c, vars = core c in
      aux (pexp_fun ~loc al None (ppat_any ~loc) c, vars)
    | Ptyp_object (l, _), Some name -> aux (obj ~loc ~name ?labels l)
    | _ -> raise_error ~loc "core type not handled %a" Pprintast.core_type c


and constr ~loc s l = match s, l with
  | "int", _ | "Int.t", _ -> eint ~loc 0, []
  | "int32", _ | "Int32.t", _ -> pexp_constant ~loc (Pconst_integer ("0", (Some 'l'))), []
  | "int64", _ | "Int64.t", _ -> pexp_constant ~loc (Pconst_integer ("0", (Some 'L'))), []
  | "float", _ | "Float.t", _ -> efloat ~loc "0.", []
  | "bool", _ | "Bool.t", _ -> ebool ~loc false, []
  | "string", _ | "String.t", _ -> estring ~loc "", []
  | "bytes", _ | "Bytes.t", _ -> evar ~loc "Bytes.empty", []
  | "list", _ | "List.t", _ -> elist ~loc [], []
  | "array", _ | "Array.t", _ -> pexp_array ~loc [], []
  | "option", _ | "Option.t", _ -> enone ~loc, []
  | "unit", _ -> eunit ~loc, []
  | "result", [ok; _err] ->
    let c, vars = core ok in
    pexp_construct ~loc (llid ~loc "Ok") (Some c), vars
  | "char", _ | "Char.t", _ -> pexp_constant ~loc (Pconst_char '\000'), []
  | "ref", [c] ->
    let c, vars = core c in
    eapply ~loc (evar ~loc "ref") [c], vars
  | _ ->
    let es, vars = acc_map core l in
    eapply ~loc (evar ~loc (em_name s)) es, vars

and constr_arg ~loc ?empty s l n =
  let e, vars = match empty with
    | Some e -> e, []
    | None -> constr ~loc s l in
  match s, l with
  | "option", [c] | "Option.t", [c] ->
    evar ~loc n, vars @ [Optional n, pvar ~loc n, c, None]
  | _ ->
    evar ~loc n,
    vars @ [Optional n, pvar ~loc n, ptyp_constr ~loc (llid ~loc s) l, Some e]

and variant l =
  match List.find_opt (fun prf -> match prf.prf_desc with
    | Rtag (_, _, []) -> true
    | _ -> false) l with
  | Some r -> begin match r.prf_desc with
    | Rtag ({txt; loc}, _, []) -> pexp_variant ~loc txt None, []
    | _ -> assert false end
  | None ->
    match List.find_opt (fun prf -> match prf.prf_desc with
      | Rtag (_, _, [_]) -> true
      | _ -> false) l with
    | Some r ->
      let {empty; _} = get_empty_attrs r.prf_attributes in
      begin match r.prf_desc with
        | Rtag ({txt; loc}, _, [c]) ->
          let c, vars = core ?empty c in
          pexp_variant ~loc txt (Some c), vars
        | _ -> assert false end
    | None ->
      match List.find_opt (fun prf -> match prf.prf_desc with
        | Rinherit _ -> true
        | _ -> false) l with
      | Some r ->
        let {empty; _} = get_empty_attrs r.prf_attributes in
        begin match r.prf_desc with
          | Rinherit c ->
            let c, vars = core ?empty c in
            c, vars
          | _ -> assert false end
      | None -> assert false

and obj ~loc ~name ?(labels=false) l =
  let has_arg =
    labels || has_arg @@ List.filter_map (fun pof -> match pof.pof_desc with
      | Oinherit _ -> None
      | Otag (name, c) -> Some (name.txt, c.ptyp_desc, pof.pof_attributes)) l in
  let l, vars = acc_map (fun pof ->
    let loc = pof.pof_loc in
    match pof.pof_desc with
    | Oinherit {ptyp_desc = Ptyp_constr ({txt;_}, _); _} ->
      let name = Longident.last_exn txt in
      begin match Hashtbl.find_opt inherit_fields name with
        | Some (l, vars) -> l, vars
        | None -> raise_error ~loc "unknown inherit fields"
      end
    | Otag (name, c) ->
      let loc = name.loc in
      let {empty; arg} = get_empty_attrs ~name:name.txt pof.pof_attributes in
      begin match arg with
        | Some arg ->
          begin match arg, c.ptyp_desc with
            | Some label, Ptyp_constr (n, [c])
              when Longident.name n.txt = "option" || Longident.name n.txt = "Option.t" ->
              [ pcf_method ~loc (name, Public, Cfk_concrete (Fresh, evar ~loc name.txt)) ],
              [ Optional label, pvar ~loc name.txt, c, None ]
            | _ ->
              let arg = match arg with None -> Nolabel | Some s -> Labelled s in
              [ pcf_method ~loc (name, Public, Cfk_concrete (Fresh, evar ~loc name.txt)) ],
              [ arg, pvar ~loc name.txt, c, None ]
          end
        | None ->
          let arg = if has_arg then Some name.txt else None in
          let c, vars = core ?empty ?arg ~name:name.txt c in
          [ pcf_method ~loc:c.pexp_loc (name, Public, Cfk_concrete (Fresh, c)) ], vars
      end
    | _ -> raise_error ~loc "unknown inherit fields"
  ) l in
  let fields = List.flatten l in
  Hashtbl.add inherit_fields name (fields, vars);
  pexp_object ~loc (class_structure ~self:(ppat_any ~loc) ~fields), vars

let record ~loc ?(labels=false) l =
  let i = same_prefix l in
  let has_arg =
    labels ||
    has_arg @@ List.map (fun pld -> pld.pld_name.txt, pld.pld_type.ptyp_desc, pld.pld_attributes) l in
  let l, vars = acc_map (fun pld ->
    let rname = pld.pld_name.txt in
    let name = String.sub rname i (String.length rname - i) in
    let {empty; arg} = get_empty_attrs ~name pld.pld_attributes in
    match arg with
    | Some arg ->
      begin match arg, pld.pld_type.ptyp_desc with
        | Some label, Ptyp_constr (n, [c])
          when Longident.name n.txt = "option" || Longident.name n.txt = "Option.t" ->
          (llid ~loc rname, evar ~loc name),
          [ Optional label, pvar ~loc name, c, None ]

        | _ ->
          let arg = match arg with None -> Nolabel | Some s -> Labelled s in
          (llid ~loc rname, evar ~loc name),
          [ arg, pvar ~loc name, pld.pld_type, None ]
      end
    | None ->
      let arg = if has_arg then Some name else None in
      let c, vars = core ?empty ?arg ~name pld.pld_type in
      (llid ~loc rname, c), vars) l in
  pexp_record ~loc l None, vars

let cons_table : (string, expression * (arg_label * pattern * core_type * expression option) list) Hashtbl.t = Hashtbl.create 512

let constructor l =
  let aux c =
    let loc = c.pcd_loc in
    let {empty; _} = get_empty_attrs c.pcd_attributes in
    match c.pcd_args with
    | Pcstr_tuple [] ->
      pexp_construct ~loc (llid ~loc c.pcd_name.txt) None, []
    | Pcstr_tuple l ->
      let l, vars = acc_map (core ?empty) l in
      pexp_construct ~loc (llid ~loc c.pcd_name.txt)
        (Some (pexp_tuple ~loc l)), vars
    | Pcstr_record l ->
      let r, vars = record ~loc l in
      pexp_construct ~loc (llid ~loc c.pcd_name.txt)
        (Some r), vars in
  let r = match List.find_opt (fun pcd -> pcd.pcd_args = Pcstr_tuple []) l with
    | Some c ->
      let loc = c.pcd_loc in
      pexp_construct ~loc (llid ~loc c.pcd_name.txt) None, []
    | None -> aux (List.hd l) in
  let more = List.filter_map (fun pcd ->
    let loc = pcd.pcd_loc in
    match get_cons_attrs pcd.pcd_attributes with
    | None -> None
    | Some n ->
      let n = match n with None -> String.uncapitalize_ascii pcd.pcd_name.txt | Some n -> n in
      match pcd.pcd_args with
      | Pcstr_tuple [] ->
        Some (n, (pexp_construct ~loc {txt=Lident pcd.pcd_name.txt; loc} None, []))
      | Pcstr_tuple [ {ptyp_desc=Ptyp_tuple l; _} ] | Pcstr_tuple l ->
        let le, vars = acc_map (fun c ->
          match c.ptyp_desc with
          | Ptyp_constr ({txt=Lident id; _}, _) when Hashtbl.mem cons_table id ->
            Hashtbl.find cons_table id
          | _ -> core c) l in
        let e = match pcd.pcd_args, le with
          | Pcstr_tuple [ _ ], [ e ] ->
            pexp_construct ~loc {txt=Lident pcd.pcd_name.txt; loc} (Some e)
          | _ ->
            pexp_construct ~loc {txt=Lident pcd.pcd_name.txt; loc} (Some (pexp_tuple ~loc le)) in
        Some (n, (e, vars))
      | _ -> Some (n, aux pcd)) l in
  r, more

let expressions ~name ?cons ?labels t =
  let loc = t.ptype_loc in
  let r, more = match t.ptype_kind, t.ptype_manifest with
    | Ptype_abstract, None -> raise_error ~loc "abstract type no handled"
    | Ptype_open, _ -> raise_error ~loc "open type not handled"
    | Ptype_abstract, Some m -> core ~name ?labels m, []
    | Ptype_variant l, _ -> constructor l
    | Ptype_record l, _ -> record ~loc ?labels l, [] in
  Option.iter (fun _ -> Hashtbl.add cons_table name r) cons;
  r, more
