open Ppxlib
open Ast_builder.Default

let verbose = match Sys.getenv_opt "PPX_EMPTY_DEBUG" with
  | None | Some "0" | Some "false" | Some "no" -> 0
  | Some s ->
    match s with
    | "true" -> 1
    | s -> match int_of_string_opt s with
      | Some i -> i
      | None -> 0

let debug ?(v=1) ?(force=false) fmt =
  if force || verbose >= v then Format.ksprintf (fun s -> Format.eprintf "%s@." s) fmt
  else Printf.ifprintf () fmt

let raise_error ~loc s = Location.raise_errorf ~loc s

let em_name name = if name = "t" then "empty" else name ^ "_empty"

let llid ~loc s = {txt=Longident.parse s; loc}
let esome e =
  let loc = e.pexp_loc in
  pexp_construct ~loc (llid ~loc "Some") (Some e)
let psome p =
  let loc = p.ppat_loc in
  ppat_construct ~loc (llid ~loc "Some") (Some p)
let enone ~loc =
  pexp_construct ~loc (llid ~loc "None") None

let str_of_structure e = Pprintast.string_of_structure e
let str_of_signature e =
  Pprintast.signature Format.str_formatter e;
  Format.flush_str_formatter ()

let rec parse_expr e = match e.pexp_desc with
  | Pexp_constant Pconst_string (s, _, _) -> `string s
  | Pexp_construct ({txt=Lident ("true"|"false" as b); _}, None) -> `bool (bool_of_string b)
  | Pexp_construct ({txt=Lident c; _}, None) -> `cons c
  | Pexp_ident {txt=Lident id; _} -> `id id
  | Pexp_tuple l -> `tuple (List.map parse_expr l)
  | _ -> `unknown

let get_expr_attr = function
  | PStr [{pstr_desc = Pstr_eval (e, _); _}] -> Some e
  | _ -> None

let get_id_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc=Pexp_constant Pconst_string (s, _, _); _}, _); _}]
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc=Pexp_ident {txt=Lident s; _}; _}, _); _}] -> Some s
  | _ -> None

type empty_attrs = {
  empty : expression option;
  arg : string option option;
}

let get_empty_attrs ?empty ?name l =
  List.fold_left (fun acc a -> match a.attr_name.txt with
    | "empty" | "mt" -> {acc with empty = get_expr_attr a.attr_payload}
    | "arg" | "mt.arg" -> {acc with arg = Some None}
    | "label" | "mt.label" ->
      (match name, get_id_attr a.attr_payload with
       | _, Some id | Some id, _ -> {acc with arg = Some (Some id)}
       | _ -> acc)
    | _ -> acc) { empty; arg = None } l

let get_cons_attrs l =
  List.find_map (fun a -> match a.attr_name.txt with
    | "cons" ->
      (match Option.map parse_expr (get_expr_attr a.attr_payload) with
       | Some (`id s | `string s) -> Some (Some s)
       | _ -> Some None)
    | _ -> None) l

let acc_map f l =
  let l, acc = List.fold_left (fun (y, a) x ->
    let x, b = f x in
    x :: y, a @ b) ([], []) l in
  List.rev l, acc

let same_prefix = function
  | [] -> 0
  | pld :: t ->
    match String.index_opt pld.pld_name.txt '_' with
    | None -> 0
    | Some i ->
      let p = String.sub pld.pld_name.txt 0 i in
      if List.fold_left (fun b pld ->
        if not b then false
        else
        let name = pld.pld_name.txt in
        match String.index_opt name '_' with
        | None -> false
        | Some i -> String.sub name 0 i = p) true t then i + 1
      else 0

let compare_arg (a1, _, _, _) (a2, _, _, _) = match a1, a2 with
  | Optional _, Optional _ -> 0
  | Optional _, _ -> -1
  | _, Optional _ -> 1
  | Labelled _, Labelled _ -> 0
  | Labelled _, _ -> -1
  | _, Labelled _ -> 1
  | _ -> 0

let has_arg l =
  List.fold_left (fun acc (name, desc, attrs) ->
    let {arg; _} = get_empty_attrs ~name attrs in
    match arg, desc with
    | Some _, _ -> true
    | _, Ptyp_var _ -> true
    | _ -> acc) false l

let add_unit_arg ~loc vars =
  match vars with
  | [] -> []
  | _ ->
    let has_arg, all_labelled =
      List.fold_left (fun (has_arg, all_labelled) (a, _, _, _) -> match a with
        | Nolabel -> true, false
        | Labelled _ -> has_arg, all_labelled
        | Optional _ -> has_arg, false) (false, true) vars in
    if has_arg || all_labelled then vars
    else
      vars @ [ Nolabel, punit ~loc, ptyp_constr ~loc {txt=Lident "unit"; loc} [], None ]
