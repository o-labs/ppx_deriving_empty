open Ppxlib
open Ast_builder.Default
open Utils

let str_gen ~loc ~path:_ (rec_flag, l) force gname cons labels =
  let aux name e vars t =
    let vars = List.sort compare_arg vars in
    let vars = add_unit_arg ~loc vars in
    let expr = List.fold_right (fun (a, p, _t, dft) e ->
      pexp_fun ~loc a dft p e)
      vars e in
    let typ = ptyp_constr ~loc (llid ~loc t.ptype_name.txt) (List.map fst t.ptype_params) in
    let typ = List.fold_right (fun (a, _, at, _) t -> ptyp_arrow ~loc a at t)
        vars typ in
    value_binding ~loc ~pat:(ppat_constraint ~loc (pvar ~loc name) typ)
      ~expr in
  let l, l_cons = List.fold_left (fun (acc, acc_cons) t ->
    let name = t.ptype_name.txt in
    let (e, vars), cons = Empty.expressions ~name ~labels ?cons t in
    let gname = match gname with None -> em_name name | Some n -> n in
    acc @ [ aux gname e vars t ],
    acc_cons @ List.map (fun (name, (e, vars)) -> aux name e vars t) cons
  ) ([], []) l in
  let l =
    let rec_flag = if List.length l < 2 then Nonrecursive else rec_flag in
    [ pstr_value ~loc rec_flag l ] in
  let l_cons = List.map (fun v -> pstr_value ~loc Nonrecursive [v]) l_cons in
  let s = match cons, l_cons with
    | Some false, _ -> l_cons
    | None, _ :: _ -> l_cons
    | _ -> l @ l_cons in
  debug ~force "%s\n" (str_of_structure s);
  s

let sig_gen ~loc ~path:_ (_rec_flag, l) gname cons labels =
  let aux name vars t =
    let vars = List.sort compare_arg vars in
    let vars = add_unit_arg ~loc vars in
    let typ = ptyp_constr ~loc (llid ~loc name) (List.map fst t.ptype_params) in
    let typ = List.fold_right (fun (a, _, at, _) t -> ptyp_arrow ~loc a at t)
        vars typ in
    value_description ~loc ~name:{txt=name; loc} ~type_:typ ~prim:[] in
  let l, l_cons = List.fold_left (fun (acc, acc_cons) t ->
    let name = t.ptype_name.txt in
    let (_e, vars), cons = Empty.expressions ~name ~labels ?cons t in
    let gname = match gname with None -> em_name name | Some n -> n in
    acc @ [ aux gname vars t ],
    acc_cons @ List.map (fun (name, (_, vars)) -> aux name vars t) cons
  ) ([], []) l in
  let l = match cons with
    | Some false -> l_cons
    | _ -> l @ l_cons in
  let s = List.map (psig_value ~loc) l in
  debug "%s\n" (str_of_signature s);
  s

let econs t =
  let f = Deriving.Args.to_func t in
  Deriving.Args.of_func (fun ctx loc x k ->
    match parse_expr x with
    | `id "cons" -> f ctx loc false k
    | `bool b -> f ctx loc b k
    | _ -> Location.raise_errorf ~loc "wrong cons argument")

let ename t =
  let f = Deriving.Args.to_func t in
  Deriving.Args.of_func (fun ctx loc x k ->
    match parse_expr x with
    | `string s | `id s -> f ctx loc s k
    | _ -> Location.raise_errorf ~loc "wrong name argument")

let () =
  let args_str = Deriving.Args.(
    empty +> flag "debug"
    +> arg "name" (ename __)
    +> arg "cons" (econs __)
    +> flag "labels"
  ) in
  let args_sig = Deriving.Args.(
    empty +> arg "name" (ename __)
    +> arg "cons" (econs __)
    +> flag "labels"
  ) in
  let str_type_decl = Deriving.Generator.make args_str str_gen in
  let sig_type_decl = Deriving.Generator.make args_sig sig_gen in
  Deriving.ignore @@ Deriving.add ~str_type_decl ~sig_type_decl "empty"
